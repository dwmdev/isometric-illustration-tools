# Isometric Illustration tools



## Instructions

This repo contains two files to aid in creating isometric vector illustrations. 

Use the file `iso-grid.svg` as a template or grid when you are making drawings. 

The file `isometric.aia` is an Adobe Illustrator Actions file that creates left, right, and top isometric transformations from flat art. You can install the Actions file from the Actions panel in Illustrator.


